package com.bellver.edd.Activitat5;

public class MainActivitat5 {

    private static LocalQueue database;

    public static void main (String[] args){

        database = new LocalQueue();
        addMessages();
        consumeMessages();

    }

    public static void addMessages(){

        for (int x = 0; x < 10; x++){

            database.push("message" + x);
        }

    }

    public static void consumeMessages(){

            while (!database.isEmpty()) {

                System.out.print(database.pop() + "\n");

            }

    }

}


