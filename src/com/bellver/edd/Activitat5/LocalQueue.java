package com.bellver.edd.Activitat5;

import java.util.ArrayList;

public class LocalQueue {

    private ArrayList<Object> data;

    public LocalQueue(){

        data = new ArrayList<>();

    }

    public void push(String newObject){

        data.add(newObject);

    }

    public Object pop(){

         return data.remove(0);

    }

    public boolean isEmpty(){

        return (data.size() <= 0);
    }

}
