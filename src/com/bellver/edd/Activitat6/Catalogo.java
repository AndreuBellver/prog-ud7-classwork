package com.bellver.edd.Activitat6;

import java.util.HashMap;

public class Catalogo {

    private HashMap<String, Producte> catalogo;

    public Catalogo() {

        this.catalogo = new HashMap<>();

    }

    public void addToCatalogo(String codi, Producte producte) {

        catalogo.put(codi, producte);

    }


    public Producte getProducte(String codi){

        return catalogo.get(codi);
    }

    public void showCatalog(){

        System.out.println("\nCATALOG:\n=========================");

        for (String key: catalogo.keySet()){

            System.out.print("\ncode: " + key + "\n" +
                    catalogo.get(key));

        }
    }

    public boolean isValidCode(String code) {

        return catalogo.containsKey(code);

    }

}