package com.bellver.edd.Activitat6;

import java.util.Scanner;

public class Menu {

    private Scanner scanner;
    private Catalogo catalogo;

    private final String OP_SHOWPRODUCT = "1";

    private final String OP_INSERTPRODUCT = "2";

    private final String OP_EXIT = "3";

    private final String OP_CLOSE = "-1";

    public Menu() {

        this.scanner = new Scanner(System.in);
        this.catalogo = new Catalogo();

    }

    public void mainMenu() {


        String opcion = "";

        do {


            System.out.println("\n\nMenu:");
            System.out.println(OP_SHOWPRODUCT + ".- Show Product.");
            System.out.println(OP_INSERTPRODUCT + ".- Insert Products.");
            System.out.println(OP_EXIT + ".- Exit.");

            opcion = scanner.next();

            if (opcion.equalsIgnoreCase(OP_SHOWPRODUCT)) {

                System.out.println("\nSHOW PRODUCT:\nCodigo:\n - exit to return.\n - all to show all. ");

                showProduct();

                mainMenu();

            } else if (opcion.equalsIgnoreCase(OP_INSERTPRODUCT)) {

                insertProduct();

            }

        } while (!opcion.equalsIgnoreCase(OP_EXIT));
    }

    private void insertProduct() {

        String codi = "0";

        while (!codi.equalsIgnoreCase(OP_CLOSE)) {

            System.out.print("\nNEW PRODUCT:\nCodi: ");
            codi = scanner.next();

            if (!codi.equalsIgnoreCase("-1")){

                System.out.print("\nDescripcio: ");
                String descripcio = scanner.next();

                if (catalogo.isValidCode(codi)){
                    System.out.println("The code allready exists, would you like to suscribe?(Y/N)");

                    String eleccio = scanner.next();

                    if (eleccio.equalsIgnoreCase("Y")){

                        catalogo.addToCatalogo(codi, new Producte(descripcio));
                    }

                } else  {

                    catalogo.addToCatalogo(codi, new Producte(descripcio));

                }
            }
        }

        catalogo.showCatalog();
        System.out.println("\n\n");

    }

    public void showProduct(){

        String respuesta;

        do {

             respuesta = scanner.next();

            if (respuesta.equalsIgnoreCase("exit")){

                return;

            }

            else if (respuesta.equalsIgnoreCase("all")){

                catalogo.showCatalog();

                return;

            } else if (catalogo.isValidCode(respuesta)) {

                catalogo.getProducte(scanner.next());

                return;

            } else {

                System.out.println("Codigo no valido. Vuelva a insertarlo.");
            }

        } while (true);

    }

}

