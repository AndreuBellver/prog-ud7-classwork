package com.bellver.edd.pacient;


import java.util.ArrayList;

public class Pacient {
    private String birthday;
    private Sex sex;
    private double height;
    private double weight;

    ArrayList <Pacient> Pacients;

    public Pacient(String birthday, Sex sex, double height, double weight) {
        this.birthday = birthday;
        this.sex = sex;
        this.height = height;
        this.weight = weight;
    }

    public String getIMC(){

        double imc =  weight/(height*height);

        if (imc < 18.5){
            return imc + "Pes insuficient";
        } else if (imc > 18.5 && imc <= 24.9){
            return imc + "Pes normal";
        } else if (imc > 24.9 && imc <= 26.9){
            return imc + "Sobrepes grau 1";
        }else if (imc > 26.9 && imc <= 29.9){
            return imc + "Sobrepes grau 2";
        } else{
            return imc + "Sobrepes grau 3";
        }
    }

    public int getEdad(){

        return  1;
    }

}
