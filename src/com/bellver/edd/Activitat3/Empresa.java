package com.bellver.edd.Activitat3;

import java.util.ArrayList;
import java.util.Scanner;

public class Empresa {

    private ArrayList<Producte> pedido;
    private ArrayList<Comanda> llistaComandes;
    private Catalogo catalogo;


    private Scanner scanner;

    public Empresa() {

        this.pedido = new ArrayList<>();
        this.llistaComandes = new ArrayList<>();
        this.catalogo = new Catalogo();

        this.scanner = new Scanner(System.in);
    }


    public void realizeComanda() {

        Comanda newComanda = new Comanda(getClient(), new Pedido().addToPedido(), getData());
        insertComanda(newComanda);
    }


    public void insertComanda(Comanda comanda) {

        llistaComandes.add(comanda);
    }

    public void showLlistaComandes() {
        for (Comanda LlistaComandes : llistaComandes) {

            System.out.print(LlistaComandes);
        }
    }

    public String getClient() {

        return scanner.next();
    }


    public String getData() {

        System.out.print("Data:");
        return scanner.next();
    }

    public void showCatalog() {
        catalogo.showCatalogo();
    }

}

