package com.bellver.edd.Activitat3;

public class Comanda {

    private String data;
    private String client;
    private Pedido pedido;

    public Comanda(String client,Pedido pedido, String data){

        this.client = client;
        this.pedido = pedido;
        this.data = data;

    }

    @Override
    public String toString() {
        return "Data: " + data + "\n" +
                "Client: " + client + "\n" +
                "Pedido: " + pedido;


    }
}
