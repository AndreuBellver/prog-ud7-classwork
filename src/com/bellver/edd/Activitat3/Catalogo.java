package com.bellver.edd.Activitat3;

import java.util.ArrayList;

public class Catalogo {

    private ArrayList<Producte> catalogo;


    public Catalogo(){

        this.catalogo = new ArrayList<>();

    }

    public void showCatalogo(){

        for (Producte Catalogo :catalogo) {

            System.out.println(Catalogo + "\n");
        }
    }
    public Producte returnProducte(String codigo){

            for (Producte LlistaProducte: catalogo) {

                if (LlistaProducte.getCodi().equalsIgnoreCase(codigo)) {

                    return  LlistaProducte;
                }
            }

            return null;
    }

    public void addProductToCatalog(Producte producte){

        catalogo.add(producte);
    }

}

