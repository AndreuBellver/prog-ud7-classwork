package com.bellver.edd.Activitat3;

public class Validadores {

    public boolean isValidProductCodeSingle(String code){

        if (code.matches("[0-999]")){
            return true;
        }
        return false;
    }
    public boolean isValidProductCodeMultiple(String code){

        if (code.matches("[0-999][ ][0-999]")){
            return true;
        }
        return false;
    }

}
