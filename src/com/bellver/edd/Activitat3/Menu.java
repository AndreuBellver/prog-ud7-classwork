package com.bellver.edd.Activitat3;

import java.util.Scanner;

public class Menu {

    private Scanner scanner;
    private Empresa mancor;

    public Menu(Empresa mancor){

        this.scanner = new Scanner(System.in);
        this.mancor = mancor;
    }

    public void mainMenu(){
        System.out.print("Sistema gestor de comandas:\n" +
                         "======================================\n\n" +
                "1.- Realizar Comanda\n" +
                "2.- Visualizar Comandas\n" +
                "3.- Visualizar Productos\n" );

        int eleccion = scanner.nextInt();

      switch (eleccion){
          case 1:
              mancor.realizeComanda();
              mainMenu();

          case 2:
              mancor.showLlistaComandes();
              mainMenu();

          case 3: mancor.showCatalog();
                  mainMenu();

      }

    }

}
