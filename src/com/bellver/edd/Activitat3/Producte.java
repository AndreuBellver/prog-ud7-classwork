package com.bellver.edd.Activitat3;

public class Producte {
    private String codi;
    private String nom;
    private double preubase;
    private int iva;
    private double descompte;


    public Producte(String codi, String nom, double preubase, int iva, double descompte) {

        this.codi = codi;
        this.nom = nom;
        this.preubase = preubase;
        this.iva = iva;
        this.descompte = descompte;
    }

    public double getPVP(){

        return (preubase * (1+(iva/100)) - preubase * (descompte/100));

    }

    public String getCodi() {
        return codi;
    }

    @Override
    public String toString() {
        return "Codi: " + codi +
                "Nom: " + nom +
                "Preu base(Sense iva): " + preubase +
                "Descompte: " + descompte +
                "Preu Final: " + getPVP();
    }
}
