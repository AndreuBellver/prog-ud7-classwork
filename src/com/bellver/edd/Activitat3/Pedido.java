package com.bellver.edd.Activitat3;

import java.util.ArrayList;
import java.util.Scanner;

public class Pedido {
    
    private ArrayList<Producte> pedido;
    private Catalogo catalogo;
    private Scanner scanner;
    private Validadores validadores;

    
    public Pedido(){

        this.pedido = new ArrayList<>();
        this.catalogo = new Catalogo();
        this.scanner = new Scanner(System.in);
        this.validadores = new Validadores();

        setFullCatalogo();

    }

    public void setFullCatalogo(){

        Producte taronjes = new Producte("0001", "taronjes", 0.55, 4, 10);
        Producte pomes = new Producte("0002", "pomes", 0.75, 4, 10);
        Producte peres = new Producte("0003", "peres", 0.45, 4, 0);

        catalogo.addProductToCatalog(taronjes);
        catalogo.addProductToCatalog(peres);
        catalogo.addProductToCatalog(pomes);
    }

    public Pedido addToPedido (){

        catalogo.showCatalogo();

        Producte producte;
        String respuesta;
        Pedido newPedido = new Pedido();

        do {

            System.out.println("Introduce el codigo del producto:(Dejar en blanco para no introducir más.)");
            respuesta = scanner.next();

            if (validadores.isValidProductCodeSingle(respuesta)){

                producte = catalogo.returnProducte(respuesta) ;

                newPedido.addToPedido(producte);

            } else if (validadores.isValidProductCodeMultiple(respuesta)){

                String[] insertProducto = respuesta.split(" ");

                producte = catalogo.returnProducte(insertProducto[0]) ;

                int quantitat = Integer.parseInt(insertProducto[1]);

                newPedido.addToPedido(producte, quantitat);

            } else {

                Messages.errorMessage();

            }

        }while (respuesta == null);

    return newPedido;
    }

    public void addToPedido(Producte producte){


            pedido.add(producte);

        }

    public void addToPedido(Producte producte, int quantitat){

        for (int x = 0; x < quantitat; x++){
        pedido.add(producte);

        }

    }

    public String showPedido(){

        String finalpedido = "";

        for (Producte Pedido :pedido) {

           finalpedido = finalpedido + Pedido + "\n";
        }
        return finalpedido;
    }

    @Override
    public String toString() {
        return showPedido();
    }
}
