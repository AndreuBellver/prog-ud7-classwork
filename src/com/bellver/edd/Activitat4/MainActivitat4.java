package com.bellver.edd.Activitat4;

public class MainActivitat4 {

    public static LocalStack data;

    public static void main(String[] args) {

        data = new LocalStack();
        showReverted("Ejemplo uso pila");

    }

    public static void showReverted(String args){



        System.out.print("\nNormal: " + args + "\n");

        for (int y = 0; y < args.length(); y++){

            data.push(args.charAt(y));

        }

        System.out.print("Reverse: " );

        while (!data.isEmpty()){

            System.out.print(data.pop());


        }

    }
}