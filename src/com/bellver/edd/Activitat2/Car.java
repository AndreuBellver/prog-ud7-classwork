package com.bellver.edd.Activitat2;

public class Car {

    private String matricula;
    private String color;
    private String model;
    private String marca;
    private int cilindrada;
    private int numPuertas;
    private String fechaMatriculacion;


    public Car(String matricula, String color, String model, String marca, int cilindrada, int numPuertas, String fechaMatriculacion) {
        this.matricula = matricula;
        this.color = color;
        this.model = model;
        this.marca = marca;
        this.cilindrada = cilindrada;
        this.numPuertas = numPuertas;
        this.fechaMatriculacion = fechaMatriculacion;
    }

    public String getMatricula() {
        return matricula;
    }

    @Override
    public String toString() {
        return "\nMatricula: " + matricula + "\n" +
                "Marca: " + marca + "\n" +
                "Model: " + model + "\n" +
                "Cilindrada: " + cilindrada;}
}
