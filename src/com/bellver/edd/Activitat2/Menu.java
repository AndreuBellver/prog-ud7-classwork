package com.bellver.edd.Activitat2;

import java.util.Scanner;

public class Menu {

    private Scanner scanner;

    private Garage garage;

    private final int OP_REGISTER = 1;
    private final int OP_PENDING = 2;
    private final int OP_REPAIRED = 3;
    private final int OP_SETASAREPAIRED = 4;
    private final int OP_SETEXITCAR = 5;
    private final int OP_EXIT = 6;

    public Menu(){
        scanner = new Scanner(System.in);
        garage = new Garage();
    }

    public void MainMenu() {

        int respuesta;

        do{

        System.out.print("\n\nMenu:" +
                "\n======" +
                "\n1.- Register New Car" +
                "\n2.- Pending to repair cars" +
                "\n3.- Repared cars" +
                "\n4.- Set car as a Repair" +
                "\n5.- Set car's exit" +
                "\n6.- Close Program\n\n");

        respuesta = scanner.nextInt();

        switch (respuesta){
            case OP_REGISTER:
                RegisterNewCar();
                break;

            case OP_PENDING:
                garage.showPendingCars();
                break;

            case OP_REPAIRED:
                garage.showRepairedCars();
                break;

            case OP_SETASAREPAIRED:
                garage.showPendingCars();
                garage.registerRepair(getMatricula());
                break;

            case OP_SETEXITCAR:
                garage.showRepairedCars();
                garage.takeOffCar(getMatricula());
                break;
        }
      } while (respuesta!=OP_EXIT);
    }

    private void RegisterNewCar(){

        Car newCar = new Car(getMatricula(),getColor(),getModel(),getMarca(),getCilindrada(),getNumPuertas(),getFechaMatriculacion());
        garage.registerNewCar(newCar);

    }

    public String getMatricula() {
        System.out.print("\nMatricula:");
        return scanner.next();
    }

    public String getColor() {
        System.out.print("Color:");
        return scanner.next();
    }

    public int getCilindrada() {
        System.out.print("Cilindrada:");
        return scanner.nextInt();
    }

    public int getNumPuertas() {
        System.out.print("Num de puertas:");
        return scanner.nextInt();
    }

    public String getFechaMatriculacion() {
        System.out.print("Fecha de Matriculacion:");
        return scanner.next();
    }

    public String getMarca() {
        System.out.print("Marca:");
        return scanner.next();
    }

    public String getModel() {
        System.out.print("Modelo:");
        return scanner.next();
    }

}
