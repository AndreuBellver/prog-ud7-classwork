package com.bellver.edd.Activitat2;

import java.util.ArrayList;

public class Garage {

    private ArrayList<Car> repaired;
    private ArrayList<Car> pending;


    public Garage() {

        this.repaired = new ArrayList<>();
        this.pending = new ArrayList<>();

    }

    public void registerRepair(String matricula){

        Car car = this.getPendingCar(matricula);

        if (car != null) {

            this.pending.remove(car);
            this.repaired.add(car);

        }
    }
    public void registerNewCar(Car car){

        this.pending.add(car);

    }

    private Car getRepairedCar(String licensePlate ){

        for (int i = 0; i< repaired.size(); i++){

            Car coche = repaired.get(i);

            if (coche.getMatricula().equalsIgnoreCase(licensePlate)){

                return coche;
            }
        }
        return null;
    }

    public Car takeOffCar(String matricula){

        Car car = this.getRepairedCar(matricula);

        if (car != null) {

            this.repaired.remove(car);
        }
        return car;
    }

    private Car getPendingCar(String matricula ){

        for (int i = 0; i< pending.size(); i++){

            Car coche = pending.get(i);

            if (coche.getMatricula().equalsIgnoreCase(matricula)){

                return coche;
            }
        }
        return null;
    }

    public void showPendingCars(){


        for (Car Pending: pending) {

            System.out.println(Pending);

        }
    }

    public void showRepairedCars(){

        for (Car Repaired: repaired) {

            System.out.println(Repaired);

        }
    }
}
